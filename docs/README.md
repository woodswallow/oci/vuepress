# Demo home/landing page

This is just a `README.md` or `index.md` displayed as the landing page.

Please head to [Markdown extension showcase][1] to get examples about how to
use the extensions supported by VuePress.

This pre-configured VuePress instance also has an [Extra set of plugins][2]
already installed and ready to be used.

<!-- This is a comment, and the following a link reference //-->
[1]: ./01_markdown-showcase.md "Markdown extension showcase"
[2]: ./02_markdown-extra-plugins.md "Extra plugins showcase"
