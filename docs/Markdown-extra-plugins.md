# Markdown extra plugins

## Chart

Source:

````md
```chart
 {
   "type": "doughnut",
   "data": {
     "datasets": [{
       "data": [10, 20, 30],
       "backgroundColor": [
         "rgba(255, 99, 132)",
         "rgba(255, 206, 86)",
         "rgba(54, 162, 235)"
       ]
     }],
     "labels": ["Red", "Yellow", "Blue"]
   }
 }
```
````

Result:

```chart
{
  "type": "doughnut",
  "data": {
    "datasets": [{
      "data": [10, 20, 30],
      "backgroundColor": [
        "rgba(255, 99, 132)",
        "rgba(255, 206, 86)",
        "rgba(54, 162, 235)"
      ]
    }],
    "labels": ["Red", "Yellow", "Blue"]
  }
}
```

## Flowchart

Source:

```md
@flowstart
st=>start: Start|past:>http://www.google.com[blank]
e=>end: End|future:>http://www.google.com
op1=>operation: My Operation|past
op2=>operation: Stuff|current
sub1=>subroutine: My Subroutine|invalid
cond=>condition: Yes
or No?|approved:>http://www.google.com
c2=>condition: Good idea|rejected
io=>inputoutput: catch something...|future

st->op1(right)->cond
cond(yes, right)->c2
cond(no)->sub1(left)->op1
c2(yes)->io->e
c2(no)->op2->e
@flowend
```

Result:

@flowstart
st=>start: Start|past:>http://www.google.com[blank]
e=>end: End|future:>http://www.google.com
op1=>operation: My Operation|past
op2=>operation: Stuff|current
sub1=>subroutine: My Subroutine|invalid
cond=>condition: Yes
or No?|approved:>http://www.google.com
c2=>condition: Good idea|rejected
io=>inputoutput: catch something...|future

st->op1(right)->cond
cond(yes, right)->c2
cond(no)->sub1(left)->op1
c2(yes)->io->e
c2(no)->op2->e
@flowend

## Mathjax

Anything between two $ characters will be treated as TeX math. The opening $ must have a non-space character immediately to its right, while the closing $ must have a non-space character immediately to its left, and must not be followed immediately by a digit. Thus, $20,000 and $30,000 won’t parse as math. If for some reason you need to enclose text in literal $ characters, backslash-escape them and they won’t be treated as math delimiters.

### Inline math expressions

Source:

```md
Euler's identity $e^{i\pi}+1=0$ is a beautiful formula in $\mathbb{R}^2$.
```

Result:

Euler's identity $e^{i\pi}+1=0$ is a beautiful formula in $\mathbb{R}^2$.

### Block of math expressions

Source:

```md
Text before math block

$$\frac {\partial^r} {\partial \omega^r} \left(\frac {y^{\omega}} {\omega}\right) 
= \left(\frac {y^{\omega}} {\omega}\right) \left\{(\log y)^r + \sum_{i=1}^r \frac {(-1)^i r \cdots (r-i+1) (\log y)^{r-i}} {\omega^i} \right\}$$

Text after math block
```

Result:

Text before math block

$$\frac {\partial^r} {\partial \omega^r} \left(\frac {y^{\omega}} {\omega}\right) 
= \left(\frac {y^{\omega}} {\omega}\right) \left\{(\log y)^r + \sum_{i=1}^r \frac {(-1)^i r \cdots (r-i+1) (\log y)^{r-i}} {\omega^i} \right\}$$

Text after math block

### Using macros

Default macros:

```js
module.exports = {
  // Arrow
  '|->': '\\mapsto',
  '<->': '\\leftrightarrow',
  '<=>': '\\Leftrightarrow',
  '==>': '\\Longrightarrow',
  '->': '\\rightarrow',
  '=>': '\\Rightarrow',
  // Dots
  '...': '\\cdots',
  // Arithmatic
  '+-': '\\pm',
  '-+': '\\mp',
  '==': '\\equiv',
  '!=': '\\ne',
  '>=': '\\geqslant',
  '<=': '\\leqslant',
  '<<': '\\ll',
  '>>': '\\gg',
  // Functions
  '\\arccot': '\\operatorname{arccot}',
  '\\arcsec': '\\operatorname{arcsec}',
  '\\arccsc': '\\operatorname{arccsc}',
}
```

Source:

```md
We have $a>n <=> a>=n+1$, if $a, n\in\Z$.
```

Result:

We have $a>n <=> a>=n+1$, if $a, n\in\Z$.

### Preset

It is also possible to modify the preset, but it has to be done at document YAML frontmatter level.

Example:

```md
---
sidebarDepth: 3
mathjax:
  presets: '\def\lr#1#2#3{\left#1#2\right#3}'
---

$$\frac\partial{\partial t} \lr({\frac{y^t}t})$$
```

## Markmap

Source:

````md
```markmap
# Fruit

## Red

- Apple
- Cherry

## Yellow

- Lemon
- Banana
```
````

Result:

```markmap
# Fruit

## Red

- Apple
- Cherry

## Yellow

- Lemon
- Banana
```

## Mermaidjs

Source:

````md
```mermaid
sequenceDiagram
  Alice->John: Hello John, how are you?
  loop Every minute
    John-->Alice: Great!
  end
```
````

Result:

```mermaid
sequenceDiagram
  Alice->John: Hello John, how are you?
  loop Every minute
    John-->Alice: Great!
  end
```
