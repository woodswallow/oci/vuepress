const path = require('path');
const fs = require('fs');
const _ = require('lodash');

/* Load external config in YAML format */
const yaml = require('js-yaml');


/* Get config */
let configData_parsed = {};
try {
  let raw_data = fs.readFileSync('.vuepress.yml', 'utf8');
  configData_parsed = yaml.load(raw_data);
} catch (e) {
  configData_parsed = {};
  console.log(e);
  console.log("Error parsing YAML data. Defaults will be used instead");
}

/* Combine default + parsed data, if apply */
let configData = _.merge({
  site: {
    title: 'NO TITLE SET',
    description: 'NO DESCRIPTION SET',
    base_url: '/'
  },
  sidebar: {},
  navbar: [],
  extra_plugins: [
    /* Decorate sidebar links when scrolling */
    [ '@vuepress/active-header-links' ],

    /* Loading progress bar */
    [ '@vuepress/nprogress' ],

    /* Reading progress bar (it needs to be after nprogress) */
    [ 'reading-progress' ],

    /* Add an icon once some scroll is done, to easy jump to top */
    [ '@vuepress/plugin-back-to-top' ],

    /* Mimic Medium zoom */
    [ '@vuepress/plugin-medium-zoom', {
      /* See: https://github.com/francoischalifour/medium-zoom#options */
      options: {
        margin: 16
      }
    }],

    /* Configure search based on headers */
    [ '@vuepress/search', {
      searchMaxSuggestions: 10
    }],

    /* Approximate reading time based on document length */
    [ 'reading-time' ],

    /* Replace SearchBox for a full-text search variation */
    [ 'fulltext-search' ],

    /* Chart */
    [ 'chart' ],

    /* Flowchart */
    [ 'flowchart' ],

    /* Mathjax */
    [ 'mathjax', {
      /* chtml or svg */
      target: 'chtml',
      showError: true
    }],

    /* Markmap */
    [ 'markmap' ],

    /* Mermaid */
    [ 'mermaidjs' ],
  ],
  webpack_aliases: {}
}, configData_parsed);

/* Debug */
console.log("Parsed configData (defaults + project config):");
console.log(yaml.dump(configData, {
  indent:      2,
  quotingType: '"',
  forceQuotes: true,
}));

/* Prepare export as VuePress expectations */
module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: configData.site.title,
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: configData.site.description,
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#base
   */
  base: configData.site.base_url,
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#dest
   */
  dest: 'public',

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    [ 'meta', { name: 'apple-mobile-web-app-capable', content: 'yes' } ],
    [ 'meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' } ],
    [ 'link', { rel: 'stylesheet', href: '/custom.css' } ]
  ],

  markdown: {
    lineNumbers: true,
    /* options for markdown-it-anchor */
    //anchor: { permalink: false },
    /* options for markdown-it-toc */
    toc: {
      includeLevel: [1, 2],
      markerPattern: /^\[\[_TOC_\]\]/im
    },
    extendMarkdown: md => {
      md.set({ typographer: true }),
      md.use(require('markdown-it-attrs')),
      md.use(require('markdown-it-footnote')),
      md.use(require('markdown-it-include'))
    }
  },

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    /* Navigation bar logo */
    logo: '/navbar-icon.png',

    /* Sidebars shown by route (frontmatter 'sidebar: auto' overrides it!) */
    sidebar: configData.sidebar,

    /* Show h2 (1), h3 (2), ... */
    sidebarDepth: 2,

    /* Navigation bar can be disabled */
    navbar: true,

    /* Navigation bar sticky links */
    nav: configData.navbar,

    /* If false, all other docs are only h1 while current one is expanded */
    displayAllHeaders: false,

    /* Add 'Last Updated' based on git commit date (handled down by plugin) */
    lastUpdated: true,

    /* Next/Prev links */
    nextLinks: true,
    prevLinks: true,

    /* Smooth Scrolling */
    smoothScroll: true,
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: configData.extra_plugins,

  /**
   * Edit the internal webpack config. It can be Object or function
   * - configureWebpack: { ... }
   * - configureWebpack: (config, isServer) => { ... }
   */
  configureWebpack: {
    resolve: {
      alias: configData.webpack_aliases
    }
  },

  ///**
  // * Each loader in the chain applies transformations to the processed resource
  // */
  //chainWebpack: (config, isServer) => {
  //  config.module
  //    .rule("md")
  //    .test(/\.md$/)
  //    /* npm install --save-dev string-replace-loader */
  //    .use("string-replace-loader")
  //    .loader("string-replace-loader")
  //    .options({
  //      multiple: [{
  //        search: "---(.*?)---",
  //        replace: (match, p1, offset, string, groups) =>
  //          `<span style="color: green;">${p1.toUpperCase()}</span>`,
  //        flags: "ig",
  //      },
  //      {
  //        search: "---color--(.+?)--((\n|.)+?)---color",
  //        replace: (match, p1, p2, offset, string) =>
  //          `<div style="background-color: orange;">Img: ${p1}<br><span style="color: green;">${p2.toUpperCase()}</span></div>`,
  //        flags: "ig",
  //      }],
  //    })
  //    .end();
  //}
}
