# Base image to be used
ARG BASE_IMAGE_NAME="registry.gitlab.com/woodswallow/hub/alpine"
ARG BASE_IMAGE_VERSION="3.14"

# Base image to be used
FROM ${BASE_IMAGE_NAME}:${BASE_IMAGE_VERSION} as installer

# Install block
RUN set -ex; \
  # Install base requirements
  apk --no-cache add \
    bash \
    curl \
    gettext \
    git \
    jq \
    yarn \
    yq \
  ; \
  # Add default group
  addgroup -g 1000 "group"; \
  # Add default user. In Alpine long-args are not valid, equivalences instead
  adduser \
    -h "/home/user" \
    -g "OCI default user" \
    -s "/bin/bash" \
    -u 1000 \
    -G "group" \
    -D \
    "user" \
  ; \
  # Create user-owned workdir
  mkdir /workdir; \
  chown user:group /workdir;

# Split installation to ease local development speed
WORKDIR /opt/vuepress
COPY package.json yarn.lock /opt/vuepress/
RUN set -ex; \
  yarn install;
COPY . /opt/vuepress
RUN set -ex; \
  # Introduce binaries into system and PATH
  ln -s /opt/vuepress/bin/vuepress /usr/local/bin/vuepress;

# Final image composition stage
FROM scratch

# Metadata for this image (as much 1-layer as possible). Shown on Hub pages
LABEL \
  # Who started and/or maintain this (single author + email)?
  maintainer="CieNTi <cienti@cienti.com>" \
  # Be descriptive about the final usage of this image
  # NOTE: Escape long lines assuming any extra space is preserved
  org.opencontainers.image.description="VuePress in a bubble"

# Default command
CMD [ "/bin/bash" ]

# Expose TCP port
EXPOSE 8080/TCP

# To create a safer image, we switch to non-root and a workdir at the end
USER 1000
WORKDIR "/workdir"

# Get alpine as seen on 'base' but with everything already installed on it
COPY --from=installer / /
