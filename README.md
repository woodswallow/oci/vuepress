# VuePress

Containerized and pre-configured VuePress with helper script, for easy and normalized project documentation.

## TL;DR; Local build and usage

Test a local build by executing:

```bash
docker build -t vuepress:local .
# Dot at the end! -------------^
```

Run with:

```bash
# Fastest: Run it as an isolated application
docker run --rm -it vuepress:local

# Recommended: Run it sharing current folder as `workdir` inside the container,
# with the same user and group id as the user who executes the command.
# By publishing port 8080 it is possible to use VuePress 'serve' mode
docker run --rm -it \
  --user "$(id -u):$(id -g)" \
  --volume "$(pwd):/workdir" \
  --publish 8080:8080 \
  vuepress:local
```

## TL;DR; Build or serve

Build a site (for production):

```bash
vuepress \
  --baseurl '/site/folder/' \
  --build 'build_dir' \
  --output 'output_dir' \
  --theme 'default'
```

Serve a site with live reload (for faster development):

```bash
vuepress \
  --baseurl '/site/folder/' \
  --build 'build_dir' \
  --output 'output_dir' \
  --theme 'default' \
  --serve
```
